+++
title = "Contribuire"
+++

La vostra presenza sará, ovviamente, il piú grande regalo. 

Ci sono comunque modi diversi per contribuire all'evento, e soprattutto alla buona riuscita del viaggio di nozze (!!).

Il piú semplice é quello di fare una donazione. Potete fare la classica busta, oppure inviare un bonifico al numero di conto corrente che metteremo a breve su questa pagina.

Se vi sentite creativi e volete dare un tocco personale all'evento, potete contribuire aggiungendo canzoni alla playlist che abbiamo creato su Spotify!

{{< map >}}
<iframe src="https://open.spotify.com/embed/playlist/3dZXcnPICr8kElA577QSXG?utm_source=generator&theme=0" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
{{< /map >}}